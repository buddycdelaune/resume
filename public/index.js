
$('#download').click(function() {
  var pdfBody = document.getElementById('pdfBody');
  modifyExport(true);
  html2canvas(document.getElementById('pdfBody'), {
    onrendered: function(canvas) {
      var imgData = canvas.toDataURL("image/jpeg", 100.0);
      generatePDF(imgData);  
      modifyExport(false);
    }
  });
});

function modifyExport(bool){
  if (bool) {
    $('#pdfApplyMargin').addClass('pdfApplyMargin');
    $('#pdfApplyMargin').removeClass('page-letter-border');
  } else {
    $('#pdfApplyMargin').removeClass('pdfApplyMargin');
    $('#pdfApplyMargin').addClass('page-letter-border');
  }
}

function generatePDF(imgData) {
  var pdf = new jsPDF();
  setTimeout(function(){
    pdf.addImage(imgData, 'JPEG',0,0);
    pdf.save('Resume-BuddyDelaune-2016.pdf'); 
  },10)
}

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});