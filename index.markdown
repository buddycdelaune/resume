<div class="container">
<div class="to-pdf" id="download" data-toggle="tooltip" title="Export to PDF"></div>
  <div id="pdfBody" class="page-container fade-in">
    <div id="pdfApplyMargin" class="page-letter page-letter-border">
      <span class="resume">R&#0233;sum&#0233;</span>
      <div class="horiz-line"></div>
      <table class="section">
        <tr>
          <td class="col text-left col-quarter">
            <table>
              <tr class=""><td>1444 Carrollton Ave</td></tr>
              <tr class=""><td>Apt 203</td></tr>
              <tr class=""><td>Metairie La, 70005</td></tr>
            </table>
          </td>
          <td class="col col-half">
            <table class="no-mp">
              <tr><td class="name">Buddy Christopher Delaune</td></tr>
              <tr><td class="nick-name"><i>"Chris"</i></td></tr>
            </table>
          </td>
          <td class="col text-right col-quarter">
            <table class="no-mp">
              <tr><td>(504) 352-0118</td></tr>
              <tr><td><a href="mailto:buddy.delaune@gmail.com"> buddy.delaune@gmail.com</a></td></tr>
              <tr><td><a href="mailto:bcdel89@yahoo.com">bcdel89@yahoo.com</a></td></tr>
            </table>
          </td>
        </tr>
      </table>
      <div class="section-header">EDUCATION</div>
        <table class="no-mp section">
          <tr>
            <td class="col col-quarter text-left">Louisiana State University</td>
            <td class="col col-half" style="font-size:.8em;">Bachelors Degree in Computer Science with a concentration in Software Development (Graduation Date May 2015)</td>
            <td class="col col-quarter text-right"><i>Baton Rouge, La</i></td>
          </tr>
        </table>
      <div class="section-header">EMPLOYMENT</div> 
      <div class="section">
        <div class="title"><i>Amies Boutique, Baton Rouge, La (2009 - 2013)</i></div>
        <div class="subtitle">Graphic Designer/IT Admin</div>
        <div class="detail">Planned and formulated IT policies/security standards, helped design data storage solutions for inventory tracking, and worked one-on-one with owners to develop logo designs, custom illustrations, ads, and website designs.</div>
      </div>
      <div class="section">
        <div class="title"><i>URS an AECOM Company, Baton Rouge, La (July 2013 - May-2015)</i></div>
        <div class="subtitle">Programmer</div>
        <div class="detail">Helped maintain and update a disaster recovery related online application storage and processing system. Acted as a helpdesk support administrator to serve the programming and data needs of coroworkers all around the country. Programming work consisted of: using scripting languages to automate daily tasks and routines, updating two front-end web apps (written with ASP.net, c#, and the web dev technologies), maintaining two native iOS (ipad only) applications (written in objective-c), and managing two sizable databases (one written in Oracle the other in SQLServer) in which my duties included performing database development tasks like creating/modifying tables, views, and procedures.</div>
      </div>
      <div class="section">
        <div class="title"><i>XenneX LLC (Sole Proprietorship), Baton Rouge, La (2014 - present)</i></div>
        <div class="subtitle">Founder/Full Stack Developer</div>
        <div class="detail">Sole Designer and Developer for mobile applications published for GooglePlay, iTunes, and the Amazon Store. Front end technologies used included: Lua, Html, CSS, Sass, AngularJs, Cordova, Ionic. GCP, and python were used for cloud/server side development, and sqlLite and Datastore as the data storage solutions. Notable projects include: </div>
        <ul class="project-list" style="margin-bottom:0px;">
        <li>Cosmic Crush (iOS, Android) <i>Published 2014</i></li>
          <li>A Layman's BAC Calculator (Android) <i>Published 2014</i></li>
          <li>Comet (iOS, Android, Amazon) <i>Published 2015</i></li>
        </ul>
      </div>
      <div class="section">
        <div class="title"><i>Leviton Manufacturing Co Inc, New Orleans, La (May 2015 - present)</i></div>
        <div class="subtitle">Software Engineer</div>
        <div class="detail">Work closely with product managers and UX designers to design and implement the smart home/business technology needs of the market and our customers all around the world using cutting edge technologies. Front-end work consists of creating dynamic mobile/web applications that can communicate with devices through means of bluetooth & wifi, but also to Leviton Cloud/Servers via RESTful web services. Back-end work consists of assisting in the development of new cloud service APIs & client libraries as needed. Technologies used include: Cordova, AngularJs, Ionic, html, Javascript, CSS, Sass, Swift, Objective-C, C, NodeJs, AWS.</div>
      </div>
      <div class="section">
        <div class="title"><i>B&A Technologies (December 2015 - present)</i></div>
        <div class="subtitle">Co-founder/CTO/Lead Full Stack Developer</div>
        <div class="detail">Mobile/Web application development. Front-end technologies include: Lua, Cordova, html, CSS, Sass, AngularJs. Back-end technologies include: GCP, Datastore, and python.  Notable Project: </div>
        <ul class="project-list" style="padding-bottom:5px;">
          <li>Lynqs (iOS, Android) <i>In progress beta 2016</i></li>
        </ul>
      </div>
      <div class="section-header">SKILLS & TRAINING</div>
      <div class="section" style="padding-bottom:5px;">
        <div class="subheader"><i>Languages & Frameworks:</i></div>
        <div class="skills-training">Java, JavaScript, jQuery, C, C#, Shell, Lua, Objective-C, Processing, Python, ASP.net, HTML, CSS, Sass, AngularJs, Cordova, PhoneGap, Ionic, Scheme, NodeJs, Bootstrap, VBA, XML, UML, Bash, Oracle, SQLServer, PLSQL, Datastore, SqlLite</div>
        <div class="subheader"><i>Technologies & Version Control:</i></div>
        <div class="skills-training">GCP, AWS, npm, bower, Corona SDK, Git, svn</div>
        <div class="subheader"><i>Environments:</i></div>
        <div class="skills-training">Vim, Visual Studio, WebStorm, IDEA, Atom, SublimeText, Excel, Access, Illustrator, Photoshop, Dream Weaver</div>
      </div>
      <div class="horiz-line"></div>
      <div class="fine-print"><i>Letters of Recommendation are available upon request.</i></div>
    </div>
  </div>
</div>
